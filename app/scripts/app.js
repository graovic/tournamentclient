'use strict';

/**
 * @ngdoc overview
 * @name tournamentsApp
 * @description
 * # tournamentsApp
 *
 * Main module of the application.
 */ 
angular
  .module('tournamentsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'facebook',
    // 'btford.socket-io',
    'LocalStorageModule',
    'ngTouch'
  ])
  .constant('Config', {
    //'backend': 'https://localhost:9001',
    //'users' : 'https://localhost:8085',
    'backend': 'https://52.25.54.127:9001',
    'users' : 'https://52.25.54.127:8085',
  })
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .when('/logout', {
        templateUrl: 'views/logout.html',
        controller: 'LogoutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .config(function(FacebookProvider) {
    FacebookProvider.init('446615132169792');
  })
  .config(function(localStorageServiceProvider) {
    localStorageServiceProvider
      .setPrefix('tournaments');
  });
