'use strict';

/**
 * @ngdoc function
 * @name tournamentsApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the tournamentsApp
 */
angular.module('tournamentsApp')
  .controller('LoginCtrl', function (Config, $scope, Facebook, $rootScope, $timeout, $http, $location, localStorageService) {
        $scope.awesomeThings = [
          'HTML5 Boilerplate',
          'AngularJS',
          'Karma'
        ];


    if (localStorageService.isSupported) {
        if (localStorageService.get('hash') !== null ) {
                $location.path('/#');
        }
    }
      // Define user empty data :/
      $scope.user = {};
      
      // Defining user logged status
      $scope.logged = false;
      
      // And some fancy flags to display messages upon user status change
      $scope.byebye = false;
      $scope.salutation = false;
      
      /**
       * Watch for Facebook to be ready.
       * There's also the event that could be used
       */
      $scope.$watch(
        function() {
          return Facebook.isReady();
        },
        function(newVal) {
          if (newVal)
            $scope.facebookReady = true;
        }
      );

    var userIsConnected = false;
      
      Facebook.getLoginStatus(function(response) {
        if (response.status == 'connected') {
          userIsConnected = true;
        }
      });

    $scope.guest = function() {
        if (localStorageService.isSupported) {
            console.log(localStorageService.get('hash'));
            if (localStorageService.get('hash') === null ) {
                $http.post(Config.users + '/guest', {})
                        .then(function(resp) {
                            localStorageService.set('hash',resp['data']['user']['hmac']);
                            localStorageService.set('userid',resp['data']['user']['userid']);
                            localStorageService.set('username', resp['data']['user']['username']);
                            localStorageService.set('token', resp['data']['user']['token']);
                            console.log(localStorageService.get('hash'));
                        }, function(resp) {
                            console.log("error for guest users");
                        });
            } else {
                alert("you are already logged in..");
            }
        }
        
    }
      
    $scope.login = function() {
      // From now on you can use the Facebook service just as Facebook api says
      Facebook.login(function(response) {
        
        console.log("This is response ", response['authResponse']);
        // Do something with response.
        console.log("this is reponse from facebook ....");
      });
    };

  /**
   * IntentLogin
   */
  $scope.IntentLogin = function() {
    console.log(userIsConnected);
    console.log("User connected... ");
    if(!userIsConnected) {
      $scope.login();
    } else {
        alert("you are already connected");
    }
  };

  /**
   * Login
   */
  $scope.login = function() {
     Facebook.login(function(response) {
        console.log("ovde response .... ");
           var data_send = {};
          console.log(response.authResponse);
          $http.post(Config.users + '/fbapi',response.authResponse)
            .then(function(resp) {
                localStorageService.set('hash',resp['data']['user']['hmac']);
                localStorageService.set('userid',resp['data']['user']['userid']);
                localStorageService.set('username', resp['data']['user']['username']);
                localStorageService.set('token', resp['data']['user']['token']);
                console.log(localStorageService.get('hash'));
            }, function(resp) { 
                console.log("err");
            })
      if (response.status == 'connected') {
        

        $scope.logged = true;
        $scope.me();
      }
    
    });
   };

    /**
    * me 
    */
    $scope.me = function() {
      Facebook.api('/me', function(response) {
        /**
         * Using $scope.$apply since this happens outside angular framework.
         */
        $scope.$apply(function() {
          $scope.user = response;
          console.log($scope.user);
          $http.post('https://localhost/')
        });
        
      });
    };


     /**
      * Logout
      */
      $scope.logout = function() {
        Facebook.logout(function() {
          $scope.$apply(function() {
            $scope.user   = {};
            $scope.logged = false;  
          });
        });
      }
      
      /**
       * Taking approach of Events :D
       */
      $scope.$on('Facebook:statusChange', function(ev, data) {
        if (data.status == 'connected') {
         
          $scope.$apply(function() {
            $scope.salutation = true;
            $scope.byebye     = false;    
          });
        } else {
          $scope.$apply(function() {
            $scope.salutation = false;
            $scope.byebye     = true;
            
            // Dismiss byebye message after two seconds
            $timeout(function() {
              $scope.byebye = false;
            }, 2000)
          });
        }

    $scope.getLoginStatus = function() {
      Facebook.getLoginStatus(function(response) {
        if(response.status === 'connected') {
          $scope.loggedIn = true;
        } else {
          $scope.loggedIn = false;
        }
      });
    };

    });
  });
