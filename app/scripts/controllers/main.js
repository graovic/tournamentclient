'use strict';

/**
 * @ngdoc function
 * @name tournamentsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the tournamentsApp
 */
angular.module('tournamentsApp')
  .controller('MainCtrl', function (Config, $scope, localStorageService, $location, $sce) {

    $scope.msg = '';

    $scope.tournamentActive = true;

    $scope.users2 = [];
    $scope.starttimefor = -1;

    var urls = [];
    $scope.games = [];

    $scope.timeleftout = 0;

    $scope.notify = '';
    
    $scope.hashid = '';
    if (localStorageService.isSupported) {
        $scope.hashid = localStorageService.get('hash');
    }
    //     urls.push({domain: $sce.trustAsResourceUrl("http://gamehouse2-stage.spingames.net/slot/wildgypsy?playerid="+$scope.hashid.substring(0,$scope.hashid.length-1)+"!wildgypsy")});
    // } else {
    //     return;
    // }

    //Needs to trust resource to be able to interpolate, see $sce documentation
     // urls.push({domain: $sce.trustAsResourceUrl("http://localhost:5000?hashid=" + $scope.hashid)});

    // urls.push({domain: $sce.trustAsResourceUrl("http://localhost:5000")});

    // $scope.iframe = urls.pop();
    $scope.iframe = "";

    console.log("Hash id ", $scope.hashid);
    // var socket2 = io('http://localhost:9001/socket.io/?hashid='+ $scope.hashid);
    var socket= io(Config.backend, {'path': '/socket.io', 'query':"hashid=" + $scope.hashid});

    socket.on('info', function(msg) {
        $scope.$apply($scope.info = msg);
    });

    socket.on('gamelist', function(data) {
        console.log(data);
        $scope.$apply($scope.games = data);
    });

    socket.on('tournamentname', function(data) {
        $scope.$apply($scope.datamsg = data);
    });
    
    socket.on('leaderboard', function(data) {
        $scope.$apply($scope.leaderboard= data);
    });

    socket.on('endoftournament', function(data) {
        $scope.$apply($scope.results = data);
    });

    socket.on('timeleft', function(data) {
        $scope.$apply($scope.timeleftout = data);
    });

    socket.on('starttime', function(data) {
        $scope.$apply($scope.starttimefor = data);
    });

    socket.on('prizepool', function(data) {
        $scope.$apply($scope.pricepool = data );
    });

    socket.on('friendgift', function(data) {
        $scope.$apply($scope.friendgift = data );
    });



    $scope.insertIframe = function(url, name) {
        console.log("insert game in iframe");
         var completeUrl = url + "?playerid="+$scope.hashid.substring(0,$scope.hashid.length-1)+"!" + name;
         console.log(completeUrl);
         $scope.iframe = $sce.trustAsResourceUrl(completeUrl);
         // $scope.iframe = completeUrl;
    }
    // socket.on('notify', function(msg) {
    //     $scope.notify = msg;
    // });

   
  });
