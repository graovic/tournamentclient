'use strict';

/**
 * @ngdoc function
 * @name tournamentsApp.controller:LogoutCtrl
 * @description
 * # LogoutCtrl
 * Controller of the tournamentsApp
 */
angular.module('tournamentsApp')
  .controller('LogoutCtrl', function ($scope, localStorageService, $location) {
    console.log("AAA");
    if (localStorageService.isSupported) {
        localStorageService.remove('hash');
        localStorageService.remove('userid');
        localStorageService.remove('username');
        localStorageService.remove('token');

        $location.path('/login');
    }
        
  });
